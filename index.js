const express = require('express');
const app = express();
const vhost = require("vhost");

app.use(vhost('netdata.*', require('./netdata').app));

app.get("/",(req,res) => {
    res.send("made by love <3")
});

const port = 8000;
app.listen(port, () => {
    console.log("app is listening on http://localhost:" + port + " ");
});
