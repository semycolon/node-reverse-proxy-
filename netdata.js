const httpProxy = require('http-proxy');
const apiProxy = httpProxy.createProxyServer();


const netdataServer = 'http://localhost:19999',
    ServerTwo = 'http://localhost:3002',
    ServerThree = 'http://localhost:3002';


var express = require('express');
var app = express();
var path = require('path');
var basicAuth = require('express-basic-auth');
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');


var router = express.Router();

app.all("/*", basicAuth({
        challenge: true,
        users: {'username': 'notsecurepassword'}
    })
    , function (req, res) {
        //   console.log('redirecting to Server1');
        apiProxy.web(req, res, {target: netdataServer});
    });

exports.app = app;